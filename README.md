### What to do

1. Install the requirements.txt with your Python interpreter (I use 3.8 but shouldn't be an issue for older ones)
2. Run the `annotator.py` with the below parameters

Run from your terminal it takes three parameters:

- class_chosen: Your annotation group that you won't be labelling (e.g. for me is `Comorbidity`)
- length_viz: Depending on your screen you can lower that or make it higher, the default is 200 (e.g. `-l 100`)

Notes
- Place your dataset in the Data folder.
- You made a mistake, with the current setting is fine just continue then you can record the index and correct it in your Excel spreadsheet by logging the index; do that after the annotation so you won't read the answer from the dataset.
- Your answers are recorded, if you terminate is fine. When you restart you will begin where you left off. 

