import json
import os
import copy
import textwrap
import pandas as pd
import json

types = ["Manifestation", "Comorbidity", "Complication", "Treatment", "Reaction"]
data_path = os.path.join(os.path.dirname(os.getcwd()), "Data")
data_path_raw = os.path.join(os.getcwd(), "Data", "Raw") #  os.path.join(os.path.dirname(os.getcwd()), "Data", "Raw")

# Extract arguments

fps = [os.path.join(data_path, type+".tsv") for type in types]

def get_negatives():
    arguments = set()
    global fps
    for fp in fps:
        print(fp)
        df = pd.read_csv(fp, sep='\t', index_col=0, header=0)
        for index, row in df.iterrows():
            for col in df.columns:
                if col[:3] == "ARG" and not pd.isna(row[col]):
                    for cnt, sub in enumerate(row[col].split(';'), start=1):
                        try:
                            sub = sub.split(':')[1][1:]
                            arguments.add(sub)
                        except IndexError:
                            continue

    fps = [os.path.join(data_path_raw, "data_cord19_" + str(counter + 1)  +".json") for counter in range(4)]
    buffer = list()
    for fp in fps:
        with open(fp, 'r') as f:
            js = json.load(f)
            for triplet in js:
                to_append = True
                has_disease=False
                for key in triplet.keys():
                    if key[:3] != "ARG":
                        continue
                    for ner in triplet[key]["ner_nested"]:
                        if not ner["text"]:
                            continue
                        if ner["text"] in arguments:
                            to_append = False
                            continue
                        if ner["type"] == "Disease":
                            has_disease = True
                if to_append and has_disease:
                    buffer.append(triplet)
    for triplet in buffer:
        yield triplet

