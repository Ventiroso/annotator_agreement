import json
import os
import copy
import textwrap
from ansi2html import Ansi2HTMLConverter

COVID = ["COVID-19", "covid", "2019-nCoV"]
comorbidities = ["comorbidity", "cancer", "diabetes", "dimer", "lymphopenia", "cardiovascular", "malnutrition",
                 "obesity", "cardiac", "hypertension", "comorbidities"]

# https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
d_ARG_colors = {
                "ARG0": '\033[95m',
                "ARG1": '\033[96m',
                "ARG2": '\033[94m',
                "ARG3": '\033[93m',
                "ARG4": '\033[92m',
                "ARG5": '\033[91m',
                }
# Start file data_cord19.2_300001_322685.json"
json_file = os.path.join(os.path.dirname(os.getcwd()), "Data", "data_cord19.2_100001_200000.json")

with open(json_file, 'r') as js:
    d = json.load(js)

counter = 0  # Where I stopped 36348
with open("test_ASCII.txt", "w") as fp:
    conv = Ansi2HTMLConverter()
    while True:
        covid_found = False
        comorbidity_found = False
        # print("Index: " + str(counter))
        d_embel = dict()
        for k, v in d[counter].items():
            if k[:3] == "ARG":
                if v['ner_id']:
                    print(d_ARG_colors[k] + k + '\033[0m')
                    print("\t" + v["text"])
                    print("\t" + v["ner_text"])
                    for cnt, arg in enumerate(d[counter][k]["ner_nested"], start=1):
                        sub = arg["text"]
                        if sub in COVID:
                            covid_found = True
                        if sub in comorbidities:
                            comorbidity_found = True

                        d_embel[k + str(cnt)] = (d[counter]["text"].index(sub),
                                                 d[counter]["text"].index(sub) + len(sub),
                                                 k)
        if not covid_found or not comorbidity_found:
            counter += 1
            continue
        # print('\033[4m' + "predicate" + '\033[0m' + ": " + d[counter]["predicate"])
        d_embel["predicate"] = (d[counter]["text"].index(d[counter]["predicate"]),
                                d[counter]["text"].index(d[counter]["predicate"]) + len(d[counter]["predicate"]))
        # Sort dictionary by values
        p_text = copy.copy(d[counter]["text"])
        for k in sorted(d_embel, key=d_embel.get, reverse=True):
            if k == "predicate":
                p_text = p_text[:d_embel[k][1]] + '\033[0m' + p_text[d_embel[k][1]:]
                p_text = p_text[:d_embel[k][0]] + '\033[4m' + p_text[d_embel[k][0]:]
            else:
                p_text = p_text[:d_embel[k][1]] + '\033[0m' + p_text[d_embel[k][1]:]
                p_text = p_text[:d_embel[k][0]] + d_ARG_colors[d_embel[k][2]] + p_text[d_embel[k][0]:]
        # print("Text:")
        if len(p_text) > 10:
            fp.write("\n")
            # temp_d = {"id": counter, "text": p_text.rstrip(), "annotations": [], "meta": {}, "annotation_approver": None}
            # fp.write(json.dumps(temp_d))
            html = conv.convert(p_text)
            fp.write(p_text.rstrip())
            print(p_text)
        # for text in textwrap.wrap(p_text, width=180):
        #     print(text)
        # print()
        # in_ = input("Press any key to continue or press 1 to quit")
        # if in_ == 1:
        #     break
        counter += 1
        # print()
    print("You stopped at index: " + str(counter))
