import os
import textwrap
import pandas as pd
import copy
import pprint
import plac
import numpy as np
from Src import negative_triplets

# https://stackoverflow.com/questions/8924173/how-do-i-print-bold-text-in-python
d_ARG_colors = {
                "ARG0": '\033[95m',
                "ARG1": '\033[96m',
                "ARG2": '\033[94m',
                "ARG3": '\033[93m',
                "ARG4": '\033[92m',
                "ARG5": '\033[91m',
                }

types = ["Manifestation", "Comorbidity", "Complication", "Treatment", "Reaction"]


def create_annotator_dfs(neg_sample=100):
    dfs = {}
    global types
    for type_ in types:
        fp = os.path.join(os.getcwd(), "Data", type_ + ".tsv")
        df = pd.read_csv(fp, sep='\t')
        df["true_label"] = type_
        dfs[type_] = (df.sample(frac=1).reset_index(drop=True))

    annotator_dfs = {}
    for type_ in types:
        annotator_dfs[type_] = None
        for key, split in dfs.items():
            if key == type_:
                continue
            try:
                annotator_dfs[type_] = annotator_dfs[type_].append(split)   # = annotator_dfs[type_].append(split[-1])
            except AttributeError:
                annotator_dfs[type_] = split
            # del split[-1]
    # Get some negative samples
    negative_df = pd.DataFrame(columns=annotator_dfs[types[0]].columns)
    for counter, triplet in enumerate(negative_triplets.get_negatives()):
        if counter > neg_sample:
            break
        negative_df.loc[counter] = None
        negative_df.loc[counter]["predicate"] = triplet["predicate"]
        negative_df.loc[counter]["text"] = triplet["text"]
        negative_df.loc[counter]["ARG0.ner_text"] = triplet["ARG0"]["ner_text"]
        negative_df.loc[counter]["ARG1.ner_text"] = triplet["ARG1"]["ner_text"] if triplet["ARG1"]["ner_text"] else np.NaN
        negative_df.loc[counter]["ARG2.ner_text"] = triplet["ARG2"]["ner_text"] if triplet["ARG2"]["ner_text"] else np.NaN
        negative_df.loc[counter]["ARG3.ner_text"] = triplet["ARG3"]["ner_text"] if triplet["ARG3"]["ner_text"] else np.NaN
        negative_df.loc[counter]["true_label"] = "Negative"

    for type_ in types:
        annotator_dfs[type_] = annotator_dfs[type_].append(negative_df, ignore_index=True)  # Infuse the negative into the df
        annotator_dfs[type_] = annotator_dfs[type_].sample(frac=1).reset_index(drop=True)  # Shuffle
        annotator_dfs[type_].to_csv(os.path.join(os.getcwd(), "Data", type_ + "_annotator_all.tsv"), sep='\t')


# @DeprecationWarning
# def dataset_creator(class_chosen, sample_size):
#     df_ = None
#     for true_label, df_label in dfs.items():
#         if true_label == class_chosen:
#             continue
#         temp_df = copy.copy(df_label.sample(sample_size).reset_index(drop=True))
#         temp_df["true_label"] = true_label
#         try:
#             df_ = df_.append(temp_df)
#         except AttributeError:
#             df_ = copy.copy(temp_df)
#     return df_


@plac.pos('class_chosen', "Label Annotated", choices=['Manifestation', 'Comorbidity', 'Complication',
                                                      'Treatment', 'Reaction'])
@plac.opt('length_viz', "Preferred column length for visual", type=int)
def main(class_chosen, length_viz=200):
    fp = os.path.join(os.getcwd(), "Data", class_chosen + "_annotator_all.tsv")
    df = pd.read_csv(fp, sep='\t', index_col=0)
    if "annotator_label" not in df.columns:
        df["annotator_label"] = None
    global types
    types.remove(class_chosen)
    types.append("Non Identifiable")
    classes = dict(enumerate(types, start=1))
    try:
        for index, row in df.iterrows():
            if not pd.isna(row["annotator_label"]):
                continue
            # df["annotator_label"] = df["annotator_label"].astype(str)  # Todo check me
            print("This is index: " + str(index))
            try:
                d_embel = dict()
                for col in df.columns:
                    if col[:3] == "ARG" and not pd.isna(row[col]):
                        print(d_ARG_colors[col[:4]] + col[:4] + '\033[0m')
                        print("\t" + row[col])
                        for cnt, sub in enumerate(row[col].split(';'), start=1):
                            sub = sub.split(':')[1][1:]
                            d_embel[sub] = (row["text"].index(sub), row["text"].index(sub) + len(sub), col[:4])
                print('\033[4m' + "predicate" + '\033[0m' + ": " + row["predicate"])
                d_embel["predicate"] = (row["text"].index(row["predicate"]),
                                        row["text"].index(row["predicate"]) + len(row["predicate"]))
                # Sort dictionary by values
                p_text = copy.copy(row["text"])
                for k in sorted(d_embel, key=d_embel.get, reverse=True):
                    if k == "predicate":
                        p_text = p_text[:d_embel[k][1]] + '\033[0m' + p_text[d_embel[k][1]:]
                        p_text = p_text[:d_embel[k][0]] + '\033[4m' + p_text[d_embel[k][0]:]
                    else:
                        p_text = p_text[:d_embel[k][1]] + '\033[0m' + p_text[d_embel[k][1]:]
                        p_text = p_text[:d_embel[k][0]] + d_ARG_colors[d_embel[k][2]] + p_text[d_embel[k][0]:]
                print("-" * length_viz)
                for text in textwrap.wrap(p_text.rstrip(), width=length_viz):
                    print(text)
            except (ValueError, IndexError):
                print('\033[4m' + "predicate" + '\033[0m' + ": " + row["predicate"])
                print("\033[31m  OMG Parsing Error （>﹏<) : below is unformatted text: \033[0m")
                for text in textwrap.wrap(row["text"].rstrip(), width=length_viz):
                    print(text)
                # continue
            except AttributeError:
                print("\033[31m _^_ Non recoverable Parsing Error _^_ \033[0m")
                continue
            print("-" * length_viz)
            print()
            while True:
                try:
                    pprint.pprint(classes)
                    in_ = input("Type one of the above class: ")
                    row["annotator_label"] = classes[int(in_)]
                    print("You chose: " + classes[int(in_)])
                    df.at[index, "annotator_label"] = classes[int(in_)]
                    df.to_csv(fp, sep='\t')
                except (ValueError, KeyError):
                    print("Please type from the below: ")
                else:
                    break
            print()
            print("-" * length_viz)
    except KeyboardInterrupt:
        print()
        print("Your file is saved at " + fp + " :), once you restart the script it will resume where you left off.")


if __name__ == '__main__':
    # create_annotator_dfs()
    plac.call(main)
